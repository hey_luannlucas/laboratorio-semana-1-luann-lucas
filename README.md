# Laboratório Semana 1 - <a href="https://gitlab.com/hey_luannlucas">Luann Lucas</a>

Aqui você encontrará minhas soluções para o Laboratório da semana 1 de programação 2

### Estrutura de Pastas 📁

Cada resolução se encontra na sua pasta correspondente ao nome do arquivo.

### ReadME 🗒️
Cada pasta (solução) possui seu arquivo README com explicações e diagramas. As imagens estão localizadas na pasta "Assets", mas elas já estão incluídas no arquivo README.
***
<br>
<a href="mailto:Luann.deSousa@jala.university">
  <img src="https://i.pinimg.com/originals/8d/94/59/8d9459f49777d0348be4a5c3f006265f.png" width="80" height="60">
</a>




