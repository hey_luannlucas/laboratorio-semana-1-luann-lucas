class Quadrado extends Retangulo {
    public Quadrado(double lado) {
        super(lado, lado);
    }
    @Override
    String getNome() {
        return "Quadrado";
    }
}
