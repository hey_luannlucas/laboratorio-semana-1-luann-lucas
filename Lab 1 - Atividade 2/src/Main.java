public class Main {
    public static void main(String[] args) {
        Figura circle = new Circulo(5);
        System.out.println(circle.toString());

        Figura rectangle = new Retangulo(15, 7);
        System.out.println(rectangle.toString());

        Figura triangle = new TrianguloEquilatero(7);
        System.out.println(triangle.toString());

        Figura square = new Quadrado(2);
        System.out.println(square.toString());
    }
}
