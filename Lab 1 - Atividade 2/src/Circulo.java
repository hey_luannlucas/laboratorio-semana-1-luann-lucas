public class Circulo extends Figura {
    private double raio;

    public Circulo(double raio) {
        this.raio = raio;
    }

    @Override
    double calcularArea() {
        return Math.PI * Math.pow(raio, 2);
    }

    @Override
    double calcularPerimetro() {
        return 2 * Math.PI * raio;
    }

    @Override
    String getNome() {
        return "Círculo";
    }

    @Override
    public String toString() {
        return getNome() + ", área: " + String.format("%.3f", calcularArea()) + ", perímetro: " + String.format("%.4f", calcularPerimetro());
    }
}

