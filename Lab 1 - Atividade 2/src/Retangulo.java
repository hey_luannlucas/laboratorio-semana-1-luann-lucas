class Retangulo extends Figura {
    private double base;
    private double altura;

    public Retangulo(double base, double altura) {
        this.base = base;
        this.altura = altura;
    }

    @Override
    double calcularArea() {
        return base * altura;
    }

    @Override
    double calcularPerimetro() {
        return 2 * (base + altura);
    }

    @Override
    String getNome() {
        return "Retângulo";
    }

    @Override
    public String toString() {
        return getNome() + ", área: " + calcularArea() + ", perímetro: " + calcularPerimetro();
    }
}
