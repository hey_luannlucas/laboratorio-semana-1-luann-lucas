class TrianguloIsosceles extends Triangulo {
    public TrianguloIsosceles(double ladoA, double ladoB) {
        super(ladoA, ladoB, ladoB);
    }

    @Override
    double calcularArea() {
        double altura = Math.sqrt(ladoA * ladoA - (ladoB * ladoB / 4));
        return (ladoB * altura) / 2;
    }

    @Override
    double calcularPerimetro() {
        return ladoA + 2 * ladoB;
    }

    @Override
    String getNome() {
        return "Triângulo Isósceles";
    }

    @Override
    public String toString() {
        return getNome() + ", área: " + calcularArea() + ", perímetro: " + calcularPerimetro();
    }
}
