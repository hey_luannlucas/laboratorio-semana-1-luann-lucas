class TrianguloEscaleno extends Triangulo {
    public TrianguloEscaleno(double ladoA, double ladoB, double ladoC) {
        super(ladoA, ladoB, ladoC);
    }

    @Override
    double calcularArea() {
        double s = (ladoA + ladoB + ladoC) / 2;
        return Math.sqrt(s * (s - ladoA) * (s - ladoB) * (s - ladoC));
    }

    @Override
    double calcularPerimetro() {
        return ladoA + ladoB + ladoC;
    }

    @Override
    String getNome() {
        return "Triângulo Escaleno";
    }

    @Override
    public String toString() {
        return getNome() + ", área: " + calcularArea() + ", perímetro: " + calcularPerimetro();
    }
}
