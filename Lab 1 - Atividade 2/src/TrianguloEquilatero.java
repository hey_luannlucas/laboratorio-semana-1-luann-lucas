class TrianguloEquilatero extends Triangulo {
    public TrianguloEquilatero(double lado) {
        super(lado, lado, lado);
    }

    @Override
    double calcularArea() {
        double s = (ladoA + ladoB + ladoC) / 2;
        return Math.sqrt(s * (s - ladoA) * (s - ladoB) * (s - ladoC));
    }

    @Override
    double calcularPerimetro() {
        return ladoA + ladoB + ladoC;
    }

    @Override
    String getNome() {
        return "Triângulo Equilátero";
    }

    @Override
    public String toString() {
        return getNome() + ", área: " + calcularArea() + ", perímetro: " + calcularPerimetro();
    }
}
