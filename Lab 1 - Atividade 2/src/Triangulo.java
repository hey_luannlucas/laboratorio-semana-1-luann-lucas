abstract class Triangulo extends Figura {
    protected double ladoA;
    protected double ladoB;
    protected double ladoC;

    public Triangulo(double ladoA, double ladoB, double ladoC) {
        this.ladoA = ladoA;
        this.ladoB = ladoB;
        this.ladoC = ladoC;
    }
}
