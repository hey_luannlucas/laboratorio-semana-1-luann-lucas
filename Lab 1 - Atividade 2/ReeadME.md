# Figuras Geométricas

Nesse trabalhamos com figuras geométricas, permitindo calcular a área e o perímetro de diferentes tipos de figuras.

## Diagrama de Classes:

![diagrama de classes](Assets/Diagrama%20de%20classes%20atv2.png)

### Figura

A classe abstrata `Figura` serve como classe base para todas as outras figuras geométricas. Ela possui três métodos abstratos:

- `calcularArea()`: calcula a área da figura.
- `calcularPerimetro()`: calcula o perímetro da figura.
- `getNome()`: retorna o nome da figura.

### Circulo

A classe `Circulo` herda da classe `Figura` e representa um círculo. Possui um atributo `raio` e implementa os métodos abstratos herdados. Além disso, sobrescreve o método `toString()` para exibir o nome da figura, a área e o perímetro.

### Quadrado

A classe `Quadrado` herda da classe `Retangulo` e representa um quadrado. Possui um construtor que recebe o lado do quadrado e implementa o método abstrato `getNome()`, retornando o nome da figura.

### Retangulo

A classe `Retangulo` herda da classe `Figura` e representa um retângulo. Possui atributos `base` e `altura` e implementa os métodos abstratos herdados. Além disso, sobrescreve o método `toString()` para exibir o nome da figura, a área e o perímetro.

### Triangulo

A classe abstrata `Triangulo` herda da classe `Figura` e serve como classe base para os diferentes tipos de triângulos. Possui atributos `ladoA`, `ladoB` e `ladoC`.

### TrianguloEquilatero

A classe `TrianguloEquilatero` herda da classe `Triangulo` e representa um triângulo equilátero. Possui um construtor que recebe o lado do triângulo e implementa os métodos abstratos herdados.

### TrianguloEscaleno

A classe `TrianguloEscaleno` herda da classe `Triangulo` e representa um triângulo escaleno. Possui um construtor que recebe os lados do triângulo e implementa os métodos abstratos herdados.

### TrianguloIsosceles

A classe `TrianguloIsosceles` herda da classe `Triangulo` e representa um triângulo isósceles. Possui um construtor que recebe os lados do triângulo e implementa os métodos abstratos herdados.

### Main

A classe `Main` é a classe principal do programa. No método `main()`, são criadas instâncias das diferentes figuras geométricas e seus atributos são definidos. Em seguida, são chamados os métodos `toString()` para exibir as informações das figuras.

## Execução:

Na classe `main()`, basta instânciar as figuras geométricas da seguinte forma:

Ex: **Círculo**:
   ```java
   Figura circle = new Circulo(5);
   ```
- Circle da classe Figura é criado e referenciado a uma instância da classe Circulo. O construtor Circulo(5) é chamado para criar um círculo com raio igual a 5
e ai será feito o calculo

Ex: **Retangulo**:
   ```java
   Figura rectangle = new Retangulo(15, 7);
   ```
- O construtor Retangulo(15, 7) é chamado para criar um retângulo com base igual a 15 e altura igual a 7 e com esses parametros será feito os calculos 

***
**O mesmo padrão é aplicado para as demais figuras geométricas**


