# Último Dígito do Fatorial

Aqui calculamos o último dígito do fatorial de um número inteiro.

## Descrição:

- O programa solicita ao usuário o número de casos de teste (T) a serem executados. 
Em seguida, para cada caso de teste, ele solicita o valor de N, calcula o último dígito do fatorial de N e exibe o resultado.
<br></br>

## Fluxograma:

![fluxograma atv 1.2](Assets/Fluxograma%20-%20atv%201.2.png)

## Execução:

1. Insira o número de casos de teste (T) quando solicitado.
2. Para cada caso de teste, insira o valor de N quando solicitado.
3. O programa exibirá o último dígito do fatorial de N para cada caso de teste.


