import java.util.Scanner;

public class UltimoDigitoFatorial {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Digite o número de casos de prova:");
        int T = input.nextInt();

        for (int i = 0; i < T; i++) {
            System.out.println("Digite o valor de N:");
            int N = input.nextInt();

            int lastDigit = CalcularUtimoDigito(N);
            System.out.println("O último dígito de " + N + "! é: " + lastDigit);
        }

    }

    public static int CalcularUtimoDigito(int N) {
        if (N == 0 || N == 1) {
            return 1;
        }

        int UltimoDigito = 1;
        for (int i = 2; i <= N; i++) {
            UltimoDigito = (UltimoDigito * i) % 10;
        }

        return UltimoDigito;
    }
}
