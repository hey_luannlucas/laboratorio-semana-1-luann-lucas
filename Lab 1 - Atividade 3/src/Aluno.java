//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

import java.util.List;

public class Aluno {
    private String nome;
    private int nota;
    private List<String> cursos;

    public Aluno(String nome, int nota, List<String> cursos) {
        this.nome = nome;
        this.nota = nota;
        this.cursos = cursos;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getNota() {
        return this.nota;
    }

    public void setNota(int nota) {
        this.nota = nota;
    }

    public List<String> getCursos() {
        return this.cursos;
    }

    public void addCurso(String curso) {
        this.cursos.add(curso);
    }
}
