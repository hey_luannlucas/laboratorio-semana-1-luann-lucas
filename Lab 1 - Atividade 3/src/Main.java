
import java.util.ArrayList;
import java.util.List;

public class Main {
    public Main() {
    }

    public static void main(String[] args) {
        Universidade universidade = new Universidade();
        Aluno aluno1 = new Aluno("Gery", 1, new ArrayList(List.of("induction")));
        universidade.addAluno(aluno1);
        Aluno aluno2 = new Aluno("Luis", 2, new ArrayList(List.of("maths", "science")));
        universidade.addAluno(aluno2);
        Aluno aluno3 = new Aluno("Raul", 1, new ArrayList(List.of("science")));
        universidade.addAluno(aluno3);
        Aluno aluno4 = new Aluno("Liz", 3, new ArrayList(List.of("maths")));
        universidade.addAluno(aluno4);
        universidade.atribuirCurso(2, "database I");
        aluno4.setNota(2);
        List<String> alunosDoCursoScience = universidade.listarAlunosPorCurso("science");
        System.out.println("Alunos do curso Science: " + String.valueOf(alunosDoCursoScience));
        universidade.listarTodosAlunos();
    }
}
