//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Universidade {
    private List<Aluno> alunos = new ArrayList();

    public Universidade() {
    }

    public void addAluno(Aluno aluno) {
        this.alunos.add(aluno);
    }

    public void atribuirCurso(int indiceAluno, String curso) {
        Aluno aluno = (Aluno)this.alunos.get(indiceAluno);
        aluno.addCurso(curso);
    }

    public void promoverNota(int indiceAluno, int novaNota) {
        Aluno aluno = (Aluno)this.alunos.get(indiceAluno);
        aluno.setNota(novaNota);
    }

    public List<String> listarAlunosPorCurso(String curso) {
        List<String> alunosPorCurso = new ArrayList();
        Iterator var3 = this.alunos.iterator();

        while(var3.hasNext()) {
            Aluno aluno = (Aluno)var3.next();
            if (aluno.getCursos().contains(curso)) {
                alunosPorCurso.add(aluno.getNome());
            }
        }

        return alunosPorCurso;
    }

    public void listarTodosAlunos() {
        Iterator var1 = this.alunos.iterator();

        while(var1.hasNext()) {
            Aluno aluno = (Aluno)var1.next();
            PrintStream var10000 = System.out;
            String var10001 = aluno.getNome();
            var10000.println("Nome: " + var10001 + ", Nota: " + aluno.getNota() + ", Cursos: " + String.valueOf(aluno.getCursos()));
        }

    }
}
