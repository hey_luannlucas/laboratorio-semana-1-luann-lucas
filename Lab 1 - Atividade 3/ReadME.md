# Aluno e Universidade - Documentação do Código

Este repositório contém os arquivos `Aluno.java` e `Universidade.java`, que implementam as classes Aluno e Universidade, respectivamente.

### Fluxograma:
![Fluxograma](Assets/Fluxograma%20-%20atv3.png)

### Diagrama de classes
![Diagrama de classes](Assets/Digrama%20de%20classes%20-%20atv3.png)


### Aluno

A classe `Aluno` representa um aluno em uma universidade. Possui os seguintes atributos:

- `nome` (String): nome do aluno.
- `nota` (int): nota do aluno.
- `cursos` (List<String>): lista de cursos em que o aluno está matriculado.

### Construtor

- `Aluno(nome: String, nota: int, cursos: List<String>)`: cria um objeto Aluno com o nome, nota e cursos fornecidos.

### Métodos

- `getNome(): String`: retorna o nome do aluno.
- `setNome(nome: String)`: define o nome do aluno.
- `getNota(): int`: retorna a nota do aluno.
- `setNota(nota: int)`: define a nota do aluno.
- `getCursos(): List<String>`: retorna a lista de cursos em que o aluno está matriculado.
- `addCurso(curso: String)`: adiciona um novo curso à lista de cursos do aluno.

### Universidade

A classe `Universidade` representa uma universidade e gerencia uma lista de alunos. Possui os seguintes atributos:

- `alunos` (List<Aluno>): lista de objetos Aluno.

### Construtor

- `Universidade()`: cria um objeto Universidade.

### Métodos

- `addAluno(aluno: Aluno)`: adiciona um objeto Aluno à lista de alunos da universidade.
- `atribuirCurso(indiceAluno: int, curso: String)`: atribui um novo curso a um aluno específico com base no índice na lista de alunos.
- `promoverNota(indiceAluno: int, novaNota: int)`: atualiza a nota de um aluno específico com base no índice na lista de alunos.
- `listarAlunosPorCurso(curso: String): List<String>`: retorna uma lista de nomes de alunos matriculados em um curso específico.
- `listarTodosAlunos()`: exibe informações de todos os alunos da universidade, incluindo nome, nota e cursos.

## Execução:




