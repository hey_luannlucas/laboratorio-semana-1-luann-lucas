# Atividade #1 - Revisão de Programação 1



O programa vai determinar em qual quadrante um ponto está com base em suas coordenadas x e y.

## Descrição do Programa

O programa solicita que o usuário insira o valor de x e o valor de y através do console. Em seguida, ele determina em qual quadrante o ponto está com base nas seguintes regras:

- Se x for negativo e y for positivo, o ponto está no Quadrante 1.
- Se x for positivo e y for positivo, o ponto está no Quadrante 2.
- Se x for negativo e y for negativo, o ponto está no Quadrante 3.
- Se x for positivo e y for negativo, o ponto está no Quadrante 4.

Se nenhuma das condições acima for atendida, o programa exibirá a mensagem "O ponto não está em nenhum quadrante válido".

## Fluxograma:
![fluxograma da soluçao](Assets/Fluxograma%20-%20atv1.1.png)

## Execução:

- Insira os valores de x e y quando solicitado.
- O programa exibirá em qual quadrante o ponto está de acordo com as coordenadas fornecidas.



