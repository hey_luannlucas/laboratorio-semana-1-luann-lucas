import java.util.Scanner;

public class SelecaoQuadrante {
    public static void main(String[] args) {
        Scanner userinput = new Scanner(System.in);

        System.out.println("Digite o valor de x:");
        int x = userinput.nextInt();

        System.out.println("Digite o valor de y:");
        int y = userinput.nextInt();

        if (x < 0 && y > 0) {
            System.out.println("O ponto está no Quadrante 1");
        } else if (x > 0 && y > 0) {
            System.out.println("O ponto está no Quadrante 2");
        } else if (x < 0 && y < 0) {
            System.out.println("O ponto está no Quadrante 3");
        } else if (x > 0 && y < 0) {
            System.out.println("O ponto está no Quadrante 4");
        } else {
            System.out.println("O ponto não está em nenhum quadrante válido");
        }
    }
}
